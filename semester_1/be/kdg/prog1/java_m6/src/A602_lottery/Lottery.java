package A602_lottery;

/**
 * @author RANSFORD ADDAI
 * ransford.addai@student.kdg.be
 * Monday 25/10/2021
 */
public class Lottery {
	public static void main(String[] args) {
		int[] lotteryNumbers = {3, 6, 17, 31, 32, 43};

		//using a regular for loop
		System.out.println("using a regular for loop");
		for (int i = 0; i < lotteryNumbers.length; i++) {
			System.out.printf("%3d", lotteryNumbers[i]);
		}
		System.out.println();
		lotteryNumbers[1] = 13;

		for (int i = 0; i < lotteryNumbers.length; i++) {
			System.out.printf("%3d", lotteryNumbers[i]);
		}

		//using for-each loop
		System.out.println("\nusing for-each loop");
		for (int i : lotteryNumbers) {
			System.out.printf("%3d", i);
		}

	}
}
