package A604_average;

/**
 * @author RANSFORD ADDAI
 * ransford.addai@student.kdg.be
 * Monday 25/10/2021
 */
public class Average {
	public static void main(String[] args) {
		double sum = 0;
		double average;

		int[] numbers = {
				12, 17, 14, 18, 13, 13, 14, 17, 17, 16, 10, 18, 13, 18, 12, 12, 10, 17, 10, 15,
				10, 11, 16, 12, 16, 11, 8, 10, 16, 14, 17, 7, 11, 10, 15, 10, 14, 8, 9, 14
		};

		for (double i : numbers) {
			sum += i;
		}
		average =  sum/numbers.length;
		System.out.printf("Average: %.3f",average);

	}
}
