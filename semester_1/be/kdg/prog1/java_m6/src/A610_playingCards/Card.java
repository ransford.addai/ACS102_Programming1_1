package A610_playingCards;

/**
 * @author RANSFORD ADDAI
 * ransford.addai@student.kdg.be
 * Monday 25/10/2021 16:07
 */
public class Card {
	private String suit;
	private String rank;

	public Card(String suit, String rank) {
		this.suit = suit;
		this.rank = rank;
	}

	public String getSuit() {
		return suit;
	}

	public String getRank() {
		return rank;
	}

	public void setSuit(String suit) {
		this.suit = suit;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}
}
