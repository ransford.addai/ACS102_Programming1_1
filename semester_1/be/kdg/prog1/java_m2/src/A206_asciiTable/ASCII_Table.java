package A206_asciiTable;

public class ASCII_Table {
	public static void main(String[] args) {
		for (char c = 32; c < 128; c++) {
			System.out.printf("    %c (%3d)", c, (int) c);

			if (c % 6 == 1) {
				System.out.println();

			}
		}
		System.out.println("---------------------------------------------------------------------------");
		for (char i = 128; i < 255; i++) {
			if (i % 6 == 1) {
				System.out.println();
			}
			System.out.printf("    %c (%3d)", i, (int) i);

		}
	}
}
