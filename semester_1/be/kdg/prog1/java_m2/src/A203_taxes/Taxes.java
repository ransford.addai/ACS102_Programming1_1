package A203_taxes;

import java.util.Scanner;

public class Taxes {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the VAT percentage:");
		double vat_per = sc.nextDouble();
		System.out.print("Enter an amount in $");
		double amount = sc.nextDouble();
		System.out.print("Make a choice (1 = inclusive, 2 = exclusive): ");
		int choice = sc.nextInt();
		if (choice == 1) {
			double inclusive = amount - vat_per;
			System.out.printf("%.1f + %.1fVAT = $%.1f", inclusive, vat_per, amount);
		} else if (choice == 2) {
			double exclusive = amount + vat_per;
			System.out.printf("%.1f + %.1fVAT = $%.1f", amount, vat_per, exclusive);

		} else {
			System.out.println("Invalid choice!");
		}

	}
}
