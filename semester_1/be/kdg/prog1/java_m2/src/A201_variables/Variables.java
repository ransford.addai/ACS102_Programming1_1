package A201_variables;

public class Variables {
	public static void main(String[] args) {
		boolean isThisAPen = true;
		char c = 'c';
		byte aByte = 1;
		short aShort = 21;
		int anInt = 546;
		long aLong = 1435467789L;
		float aFloat = 0.02F;
		double aDouble = 23.0;
		System.out.println("boolean: " + isThisAPen);
		System.out.println("Char: " + c);
		System.out.println("byte: " + aByte);
		System.out.println("short: " + aShort);
		System.out.println("int: " + anInt);
		System.out.println("long: " + aLong);
		System.out.println("float: " + aFloat);
		System.out.println("double: " + aDouble);
	}

}
