package A210_digits_v1;

import java.util.Scanner;

public class Digits_v1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		while (true) {
			System.out.println("Enter 4-digits (0-9)!");
			System.out.print("The first digits: ");
			int firstDigits = sc.nextInt();
			System.out.print("The second digits: ");
			int secondDigits = sc.nextInt();
			System.out.print("The third digits: ");
			int thirdDigits = sc.nextInt();
			System.out.print("The fourth digits: ");
			int fourthDigits = sc.nextInt();
			if (firstDigits == -1 || secondDigits == -1 || thirdDigits == -1 || fourthDigits == -1) {
				return;
			} else {
				int number = firstDigits * 1000 + secondDigits * 100 + thirdDigits * 10 + fourthDigits;
				System.out.println("The number is " + number);
			}
		}
	}
}
