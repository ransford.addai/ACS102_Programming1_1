package A212_counting;

public class Counting {
	public static void main(String[] args) {
		final int MAX = 10;
		int ascending = 0;
		int descending = MAX;
		while (ascending < MAX) {
			ascending++;
			System.out.println(ascending + " - " + descending);
			descending--;
		}
	}
}
