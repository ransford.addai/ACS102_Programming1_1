package A408_substring_extra;

import java.util.Scanner;

public class Substring_extra {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a sentence: ");
		String sentence = sc.nextLine();
		int counter = 0;
		int appearance = 0;
		for (int i = 0; i < sentence.length(); i++) {
			counter++;
			if (counter < sentence.length()) {
				if (sentence.substring(i, counter + 1).equals("ou")) {
					appearance++;
				}
			}
		}
		System.out.printf("Substring \"ou\" appears %d time(s).", appearance);

	}
}
