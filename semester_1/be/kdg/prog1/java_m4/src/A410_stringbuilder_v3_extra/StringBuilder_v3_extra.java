package A410_stringbuilder_v3_extra;

import java.util.Scanner;

public class StringBuilder_v3_extra {
	public static void main(String[] args) {
		final int AMOUNT = 5;
		Scanner sc = new Scanner(System.in);
		StringBuilder builder = new StringBuilder();
		String word;

		// Read AMOUNT words and append each word to the StringBuilder.
		// Use a for-loop and 'printf'.
		for (int i = 1; i <= AMOUNT; i++) {
			System.out.printf("Enter word %d: ", i);
			word = sc.nextLine();
			builder.append(word).append(" ");
		}
		// Print the content of 'builder'.
		System.out.printf("Content of builder: %s\n", builder);

		// Create a copy of the StringBuilder object and name it 'copy'. Make sure it contains
		// the same content as the original StringBuilder.
		StringBuilder copy = new StringBuilder(builder);

		// Print the content of 'copy'.
		System.out.printf("Content of copy: %s\n", copy);

		// Now check if 'builder' has the same content as 'copy'. Try '==' as well as the 'equals' method.
		if (builder == copy) {
			System.out.printf("Comparison with == results in: %s\n", true);
		} else {
			System.out.printf("Comparison with == results in: %s\n", false);
		}

		// Note: unlike 'String', 'StringBuilder' doesn't actually have an implementation of the 'equals'
		// method. Yet, we can still call the 'equals' method on objects of type StringBuilder.
		// This will be explained later on! (You might want to take a look at the 'Object' class.)
		if (builder.equals(copy)) {
			System.out.printf("Comparison with equals results in: %s\n", true);
		} else {
			System.out.printf("Comparison with equals results in: %s\n", false);
		}

		// Convert builder to upper case without using the String class and without creating a new StringBuilder.
		// Hint: use an ASCII table.
		System.out.printf("Upper case: %s\n", builder.toString().toUpperCase());
	}
}

