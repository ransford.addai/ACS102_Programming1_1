package A407_palindrome;

import java.util.Scanner;

public class Palindrome {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a word: ");
		String word = sc.nextLine();
		StringBuilder sb = new StringBuilder(word);
		if (word.equals(sb.reverse().toString())) {
			System.out.printf("%s is a palindrome", word);
		} else {
			System.out.printf("%s is a not palindrome", word);

		}

	}
}
