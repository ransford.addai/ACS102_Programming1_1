package A508_perfectNumber_extra;

import java.util.Scanner;

/**
 * @author RANSFORD ADDAI
 * ransford.addai@student.kdg.be
 * Monday 18/10/2021
 */
public class TestPerfectNumber {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int number = sc.nextInt();
		System.out.println(PerfectNumber_Extra.getPerfectNumber(number));
	}
}




