package A504_operations;
/*
 * @author: RANSFORD ADDAI
 * Monday 18/10/2021
 * */
public class Operations {
	private final int numberOne;
	private final int numberTwo;

	public Operations(int numberOne, int numberTwo) {
		this.numberOne = numberOne;
		this.numberTwo = numberTwo;
	}

	public void sum() {
		System.out.println("The sum is " + (numberOne + numberTwo));

	}

	public void difference() {
		System.out.println("The difference is " + (numberOne - numberTwo));
	}

	public void product() {
		System.out.println("The product is " + numberOne * numberTwo);

	}

	public void quotient() {
		System.out.printf("The quotient is %.2f",(double) (numberOne / numberTwo));
	}


}
