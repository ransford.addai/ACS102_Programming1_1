package A506_salesPerson;
/**
 * @author RANSFORD ADDAI
 * ransford.addai@student.kdg.be
 * Monday 18/10/2021
 */
public class SalesPerson {
	private String name;
	private double revenue;

	public SalesPerson(String name, double revenue) {
		this.name = name;
		this.revenue = revenue;
	}

	public String getName() {
		return name;
	}

	public double getRevenue() {
		return revenue;
	}

	public void hasMoreRevenue(SalesPerson s1, SalesPerson s2, SalesPerson s3) {
		if (s1.getRevenue() > s2.getRevenue() && s1.getRevenue() > s3.getRevenue()) {
			System.out.printf("Our top earner is %s", s1.getName());
		} else if (s2.getRevenue() < s3.getRevenue()) {
			System.out.printf("Our top earner is %s", s3.getName());
		} else {
			System.out.printf("Our top earner is %s", s2.getName());
		}
	}

	@Override
	public String toString() {
		return getName() + " " + getRevenue();
	}
}
