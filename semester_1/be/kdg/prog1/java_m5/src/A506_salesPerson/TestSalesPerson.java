
package A506_salesPerson;

import java.util.Scanner;

/**
 * @author RANSFORD ADDAI
 * ransford.addai@student.kdg.be
 * Monday 18/10/2021
 */
public class TestSalesPerson {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		String name;
		double revenue ;

		name="Jan";
		System.out.printf("Enter %s's revenue: ",name);
		SalesPerson one = new SalesPerson(name, scanner.nextDouble());

		name="Laetitia";
		System.out.printf("Enter %s's revenue: ",name);
		SalesPerson two = new SalesPerson(name, scanner.nextDouble());

		name="Lotte";
		System.out.printf("Enter %s's revenue: ",name);
		SalesPerson three = new SalesPerson(name, scanner.nextDouble());
		// TODO: Print out the name of the top earner!

		one.hasMoreRevenue(one, two, three);

	}
}
