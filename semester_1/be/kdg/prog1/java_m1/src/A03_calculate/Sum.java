package A03_calculate;

import java.util.Scanner;

public class Sum {
	public static void main(String[] args) {
		int sum;
		int first;
		int second;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number");
		first = sc.nextInt();
		System.out.println("Enter another number");
		second = sc.nextInt();
		sum = first + second;
		System.out.println("The sum " + sum);
	}
}
