package A07_bmi;

import java.util.Scanner;

public class BMI_v1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Dear patient, this program will calculate your BMI");
		System.out.print("Enter your weight in kilograms: ");
		double weight = sc.nextDouble();
		System.out.print("Enter your length in meters: ");
		double length = sc.nextDouble();
		double bmi = (weight / length * length);
		System.out.println("Your BMI is : " + bmi);

	}
}
