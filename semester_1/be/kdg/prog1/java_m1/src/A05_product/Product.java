package A05_product;

import java.util.Scanner;

public class Product {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a number: ");
		int number = sc.nextInt();
		System.out.println("Enter another number: ");
		int anotherNumber = sc.nextInt();
		System.out.println("Enter a final number: ");
		int finalNumber = sc.nextInt();
		int product = number * anotherNumber * finalNumber;
		System.out.println("The product is: " + product);

	}
}
