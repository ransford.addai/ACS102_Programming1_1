package A08_higherLower;

import java.util.Scanner;

public class HigherLower {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int constant = 245;
		while (true) {
			System.out.println("Enter a number: ");
			int number = sc.nextInt();
			if (number > constant) {
				System.out.println("Higher!");
			} else if (number < constant) {
				System.out.println("Lower!");
			} else {
				System.out.println("Congratulations, your guess is correct");
				return;
			}

		}
	}
}
