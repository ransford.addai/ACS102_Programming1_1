package A305_factorial;

public class Factorial {
	public static void main(String[] args) {
		long l = 1;
		for (int i = 1; i <= 20; i++) {
			l *= i;
			System.out.println( i + "! = " + l);
		}
	}
}
